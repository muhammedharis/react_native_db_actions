import 'react-native-gesture-handler'

import React, {useEffect} from 'react'
import {Button, View, Text} from 'react-native'

import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'

import {QueryClient, QueryClientProvider} from 'react-query'

import {openDatabase} from 'react-native-sqlite-storage'

var db = openDatabase({name: 'UserDatabase.db'})

import Home from './pages/Home'
import Details from './pages/Details'

const Stack = createStackNavigator()
const queryClient = new QueryClient()

const App = () => {
  useEffect(() => {
    db.transaction(function (txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='table_user'",
        [],
        function (tx, res) {
          console.log('item:', res.rows.length)
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS table_user', [])
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(20), user_name VARCHAR(20), user_contact INT(10), email_address VARCHAR(20), user_address_street VARCHAR(30), user_address_suite VARCHAR(30), user_address_city VARCHAR(30), user_address_zipcode VARCHAR(30), website VARCHAR(20), company_details_name VARCHAR(30), company_details_catchPhrase VARCHAR(30), company_details_bs VARCHAR(30), profile_image VARCHAR(100))',
              [],
            )
          }
        },
      )
    })
  }, [])

  return (
    <NavigationContainer>
      <QueryClientProvider client={queryClient}>
        <Stack.Navigator initialRouteName='HomeScreen'>
          <Stack.Screen
            name='Home'
            component={Home}
            options={{
              title: 'User List',
              headerStyle: {
                backgroundColor: '#f4511e',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen
            name='Details'
            component={Details}
            options={{
              title: 'User Details',
              headerStyle: {
                backgroundColor: '#f4511e',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
        </Stack.Navigator>
      </QueryClientProvider>
    </NavigationContainer>
  )
}

export default App
