import React, {useEffect, useState} from 'react'
import {View, Text, SafeAreaView, StyleSheet, FlatList} from 'react-native'

import {data} from './Data'

import Card from './Card'

import axiosInstance from './Interceptor'

import {useQuery} from 'react-query'

import {openDatabase} from 'react-native-sqlite-storage'
import {ActivityIndicator} from 'react-native-paper'

var db = openDatabase({name: 'UserDatabase.db'})

let register_user = UserDataa => {
  db.transaction(function (tx) {
    tx.executeSql(
      'INSERT INTO table_user (name, user_name, user_contact, email_address, user_address_street, user_address_suite, user_address_city, user_address_zipcode, website, company_details_name, company_details_catchPhrase, company_details_bs, profile_image) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)',
      [
        UserDataa.name,
        UserDataa.username,
        UserDataa.phone,
        UserDataa.email,
        UserDataa.address.street,
        UserDataa.address.suite,
        UserDataa.address.city,
        UserDataa.address.zipcode,
        UserDataa.website,
        UserDataa.company.name,
        UserDataa.company.catchPhrase,
        UserDataa.company.bs,
        UserDataa.profile_image,
      ],
      (tx, results) => {
        console.log('Results', results.rowsAffected)
        if (results.rowsAffected > 0) {
          console.log('Success')
        } else console.log('fail')
      },
    )
  })
}

const Home = ({navigation}) => {
  async function getUserData () {
    const myRes = await axiosInstance.get()
    let data = myRes.data

    console.log('======1111', data)

    return {userData: data}
  }
  const userDataa = useQuery([], () => getUserData())

  let [flatListItems, setFlatListItems] = useState([])

  if (userDataa.isLoading) {
    console.log('Loading')
  }

  if (userDataa.data) {
    console.log('datttaaaa', userDataa.data)
    userDataa.data.userData.map((item, index) => {
      register_user(item)
    })
  }

  if (userDataa.isError) {
    console.log('errrrrroooo', userDataa.isError)
  }
  useEffect(() => {
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM table_user', [], (tx, results) => {
        var temp = []
        for (let i = 0; i < results.rows.length; ++i)
          temp.push(results.rows.item(i))
        setFlatListItems(temp)
      })
    })
  }, [])

  const renderItem = ({item}) => {
    return (
      <Card
        itemData={item}
        onPress={() => {
          navigation.navigate('Details', {userDetails: item})
        }}
      />
    )
  }

  const UserList = () => {
    return (
      <FlatList
        style={{width: '100%'}}
        data={flatListItems}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
      />
    )
  }

  const Loader = () => {
    if (userDataa.isLoading) {
      return <ActivityIndicator />
    } else {
      return null
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.innerContainer}>
        <Loader />
        <UserList />
      </View>
    </SafeAreaView>
  )
}

export default Home

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  innerContainer: {
    width: '95%',
    alignSelf: 'center',
    flex: 1,
  },
})
