import React from 'react'
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Platform,
} from 'react-native'

import {Avatar} from 'react-native-paper'

const Card = ({itemData, onPress}) => {
  console.log('dhvshjdbsdcbjhsjd', itemData)
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.card}>
        <View style={styles.cardInfo}>
          <View style={styles.cardDetailsContainer}>
            <View style={styles.infoContainer}>
              <View style={styles.detailsContainer}>
                <Avatar.Image
                  source={{
                    uri: itemData.profile_image,
                  }}
                  size={60}
                  backgroundColor='#333'
                />
                <View style={styles.nameContainer}>
                  <Text style={styles.detailsText}>{itemData.name}</Text>
                  <Text style={[styles.detailsText, {color: 'gray'}]}>
                    {itemData.company_details_name}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  )
}
export default Card

const styles = StyleSheet.create({
  card: {
    marginTop: 15,
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  cardInfo: {
    padding: 10,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    borderWidth: 1,
    borderRadius: 8,
    backgroundColor: '#fff',
  },
  cardTitle: {
    fontWeight: 'bold',
  },
  cardDetails: {
    fontSize: 12,
    color: '#444',
  },
  cardDetailsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  dateText: {
    fontSize: 14,
    color: '#222',
    ...Platform.select({
      ios: {
        fontFamily: 'GillSans',
        fontWeight: '600',
      },
      android: {
        fontFamily: 'SourceSansPro-Regular',
      },
    }),
    textAlign: 'center',
    marginVertical: 4,
  },
  infoContainer: {
    width: '100%',
  },
  actionContainer: {
    alignItems: 'flex-end',
  },
  infoText: {
    fontSize: 14,
    color: '#222',
    ...Platform.select({
      ios: {
        fontFamily: 'GillSans',
        fontWeight: '600',
      },
      android: {
        fontFamily: 'SourceSansPro-Regular',
      },
    }),
  },

  detailsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginVertical: 5,
  },
  nameContainer: {
    // alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
  },
  detailsText: {
    fontSize: 15,
    color: '#222',
    ...Platform.select({
      ios: {
        fontFamily: 'GillSans',
      },
      android: {
        fontFamily: 'SourceSansPro-Regular',
      },
    }),
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '40%',
  },
})
