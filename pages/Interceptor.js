import axios from 'axios';

export default axios.create({
  baseURL: 'https://www.mocky.io/v2/5d565297300000680030a986',
});
