import React from 'react'
import {
  View,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Platform,
  StatusBar,
  TouchableOpacity,
  Linking,
} from 'react-native'

import {Avatar, Title, Caption, Text, TouchableRipple} from 'react-native-paper'

const Details = ({route, navigation}) => {
  const Buyer = route.params.userDetails

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View style={styles.userInfoSection}>
          <View
            style={{
              //   flexDirection: 'row',
              marginTop: 10,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Avatar.Image
              source={{
                uri: Buyer.profile_image,
              }}
              size={80}
              backgroundColor='#333'
            />
            <View style={{}}>
              {Buyer.name ? (
                <Title
                  style={[
                    styles.title,
                    {
                      marginTop: 15,
                      marginBottom: 5,
                    },
                  ]}>
                  {Buyer.name}
                </Title>
              ) : (
                <Title
                  style={[
                    styles.title,
                    {
                      marginTop: 15,
                      marginBottom: 5,
                    },
                  ]}>
                  Nill
                </Title>
              )}
              <Caption
                style={{
                  lineHeight: 15,
                  textAlign: 'center',
                  ...Platform.select({
                    ios: {
                      fontSize: 14,
                      fontFamily: 'GillSans',
                    },
                    android: {
                      fontSize: 14,
                      fontFamily: 'SourceSansPro-SemiBold',
                    },
                  }),
                }}>
                {Buyer.user_name}
              </Caption>
            </View>
          </View>
        </View>
        <View style={styles.userInfoSection}>
          <TouchableRipple
            onPress={() => {
              Platform.OS !== 'android'
                ? Linking.openURL(`telprompt:` + Buyer.user_contact)
                : Linking.openURL(`tel:` + Buyer.user_contact)
            }}>
            <View style={styles.row}>
              {Buyer.user_contact ? (
                <Text style={styles.infoTextStyle}>{Buyer.user_contact}</Text>
              ) : (
                <Text style={styles.infoTextStyle}>Nill</Text>
              )}
            </View>
          </TouchableRipple>

          <TouchableRipple
            onPress={() => Linking.openURL('mailto:' + Buyer.email_address)}>
            <View style={styles.row}>
              {Buyer.email_address ? (
                <Text style={styles.infoTextStyle}>{Buyer.email_address}</Text>
              ) : (
                <Text style={styles.infoTextStyle}>Nill</Text>
              )}
            </View>
          </TouchableRipple>
        </View>

        <View style={styles.infoBoxWrapper}></View>

        <View style={styles.innerContainer}>
          <View style={styles.buyerInfoContainer}>
            <View
              style={{
                paddingLeft: 1,
                borderLeftWidth: 2,
                borderLeftColor: '#333',
              }}>
              <Text style={styles.headerText}>Address</Text>
            </View>
            <View style={{marginTop: 10}}>
              <Text style={styles.infoText}>{Buyer.user_address_street}</Text>
              <Text style={styles.infoText}>{Buyer.user_address_suite}</Text>
              <Text style={styles.infoText}>{Buyer.user_address_city}</Text>
              <Text style={styles.infoText}>{Buyer.user_address_zipcode}</Text>
            </View>
          </View>

          <View style={styles.buyerInfoContainer}>
            <View
              style={{
                paddingLeft: 1,
                borderLeftWidth: 2,
                borderLeftColor: '#333',
              }}>
              <Text style={styles.headerText}>Company Details</Text>
            </View>
            <View style={{marginTop: 10}}>
              <Text style={styles.infoText}>{Buyer.company_details_name}</Text>
              <Text style={styles.infoText}>
                {Buyer.company_details_catchPhrase}
              </Text>
              <Text style={styles.infoText}>{Buyer.company_details_bs}</Text>
            </View>
          </View>

          <View style={styles.buyerInfoContainer}>
            <Text style={styles.infoText}>Website : {Buyer.website}</Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

export default Details

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 24,
    ...Platform.select({
      ios: {
        fontFamily: 'GillSans',
      },
      android: {
        fontFamily: 'SourceSansPro-Regular',
      },
    }),
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 25,
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoTextStyle: {
    color: '#333',
    ...Platform.select({
      ios: {
        fontFamily: 'GillSans',
      },
      android: {
        fontFamily: 'SourceSansPro-Regular',
      },
    }),
  },
  infoBoxWrapper: {
    borderBottomColor: '#dddddd',
    borderBottomWidth: 1,
    marginBottom: 20,
  },
  innerContainer: {
    width: '92%',
    alignSelf: 'center',
    flex: 1,
  },
  buyerInfoContainer: {
    borderWidth: 1,
    borderRadius: 8,
    // borderColor: '#e5e5e5',
    padding: 10,
    marginBottom: 10,
    backgroundColor: '#fff',
    shadowColor: '#999',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    borderColor: 'gray',
  },
  headerText: {
    color: '#333',
    marginLeft: 5,
    ...Platform.select({
      ios: {
        fontFamily: 'GillSans',
      },
      android: {
        fontFamily: 'SourceSansPro-Regular',
      },
    }),
  },
  nameText: {
    textAlign: 'left',
    color: '#333',
    marginBottom: 1,
    ...Platform.select({
      ios: {
        fontFamily: 'GillSans',
      },
      android: {
        fontFamily: 'SourceSansPro-Regular',
      },
    }),
  },
  infoText: {
    textAlign: 'left',
    fontSize: 14,
    color: '#333',
    ...Platform.select({
      ios: {
        fontFamily: 'GillSans',
      },
      android: {
        fontFamily: 'SourceSansPro-Regular',
      },
    }),
    marginBottom: 1,
  },
})
